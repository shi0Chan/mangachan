#!/bin/python

import requests
from bs4 import BeautifulSoup
import os
import clipboard
from multiprocessing import Process


#   Данный скрипт автоматизирует скачивание манги с сайта http://mangachan.me, 
#   принимая на ввод ссылку страницы скачивания манги. Создаёт директорию, 
#   создаёт каталоги для всех глав, скачивает архивы, 
#   распаковывая в одноимённом имени архива каталоге и удаляет архивы после архивации. 
#   Обработка ошибок не совсем корректна. 
#   Принимает ссылки типа "http://mangachan.me/download/33031-shuukan-shounen-girl.html", 
#   но в дальнейшем можно преобразовывывать ссыллки с главной страницы манги.

def write(tag, mangaName):
    fTag = tag.find("a")
    bLink = fTag.get("href")
    chNameZip = fTag.getText()
    filename = "/".join([mangaName, chNameZip])
    pathname = ".".join(filename.split(".")[0:-1])
    if pathname.split("/")[-1] in os.listdir(mangaName):
        return 1
    fullLink = clearLink + bLink
    print(fullLink)
    r = requests.get(fullLink, headers={"referer": manga_link})
    saveFile(filename, r.content)
    unzip(filename, pathname)
    os.remove(filename)

def getMangaName(soup, link):
    table = soup.find("div", id="right")
    elem = table.findAll("div")[1].find("span")
    name = elem.getText()
    if name == "":
        return link.split("/")[-1].split('.')[0]
    return name

def saveFile(name, data):
    f = open(name, "ab")
    f.write(data)
    f.close()

def unzip(file, path):
    ras = file.split(".")[-1]
    file = file.replace(" ", "\ ")
    if ras == "zip":
        command = "./unzip {} -x -d \"{}\"".format(file, path)
    elif ras == "rar":
        os.mkdir(path)
        command = "./unrar e {} {}".format(file, path)
    os.system(command)

mangaBlink = clipboard.paste()
if not mangaBlink.startswith("http"):
    manga_link = input("Скопируйте ссылку на мангу...\n")
else:
    manga_link = mangaBlink

if manga_link.split("/")[3] == "manga":
    manga_link = manga_link.replace("/manga/", "/download/")

clearLink = "/".join(manga_link.split("/")[0:3])
mangaPage = requests.get(manga_link)
if mangaPage.status_code == 404:
    print("Error! Page not found!")
    exit()

soup = BeautifulSoup(mangaPage.text, 'html.parser')
mangaName = getMangaName(soup, manga_link)
tableLinks = soup.find("table", id="download_table")
tagLinks = tableLinks.findAll("tr")[:0:-1]

if mangaName not in os.listdir("."):
    os.mkdir(mangaName)

for tag in tagLinks:
    p = Process(target=write, args=(tag,mangaName,))
    p.start()